package com.barpoltech.freesms;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;

public class MyPreferences {
    private SharedPreferences p;
    private Context c;
    public static final String SharedPrefsId = "com.barpoltech.freesms.prefs";

    public MyPreferences(Context c) {
        this.c = c;
        p = c.getSharedPreferences(SharedPrefsId, Context.MODE_PRIVATE);
    }

    public Boolean contains(String key) {
        return p.contains(key);
    }

    public String get(String key) {
        return p.getString(key, "");
    }

    public void set(String key, String value) {
        SharedPreferences.Editor editor = p.edit();
        editor.putString(key, value);
        editor.commit();
    }

}
